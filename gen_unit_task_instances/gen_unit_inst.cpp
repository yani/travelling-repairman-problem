/* Generate instances of Unit Time Tasks
 * Input Parameters:
 *      range_min: min num of task to generate, int
 *      range_max: max num of task to generate, int
 *      iNumTests: number of tests, int
 *      path: directory where will be generated files, string
 *      random1: min range required to generate random numbers
 *      random2: max range required to generate random numbers
 * Execution: ./main.o range_min range_max iNumTests path random1 random2
 *
 * Yani. December 2014.
*/

#include <iostream>
#include <fstream>
#include <string>
#include <stdio.h>
#include<time.h>
#include <cstdlib>
using namespace std;

int main(int argc, char** argv)
{
    /* initialize random seed: */
    srand (time(NULL));
    int range_min = atoi(*(argv+1));
    int range_max = atoi(*(argv+2));
    int iTests = atoi(*(argv+3));
    string path = *(argv+4);

    int random1 = atoi(*(argv+5));
    int random2 = atoi(*(argv+6));

    string name_file = "";
    string aux = "";
    int i=0, j=0, iNumTasks;

    cout << "Number of instances (tasks) to be generated: ";
    cout<<range_min<<" to "<<range_max ;
    cout <<endl<< "Number of tests randomness: ";
    cout<<iTests;
    cout <<endl<< "Directory to store the files: ";
    cout<<path;

    for(iNumTasks=range_min; iNumTasks<=range_max; iNumTasks++){
        aux = "";
        aux = path;
        aux += "/tasks";
        aux += to_string(iNumTasks);
        aux += "__";
        for(i=1; i<= iTests; i++){
            /* Creates many files as number of tests indicated. */
            name_file ="";
            name_file +=aux;
            name_file +=to_string(i); //sprintf(tmp, "%d", i);  Another way to do same
            name_file +=".txt";
            //cout<<name_file<<endl;
            ofstream myfile(name_file.c_str());
            myfile<<"c File #"<<i<<" generated.";  //first line (comment line)
            myfile<<endl;
            myfile<<"p GreedyComparisonExhaustive "<<iNumTasks<<endl; // p line to indicate num of tasks

            /* creates many tasks randomness as number of tasks indicated in iNumTasks. Format: */
            /* d_i w_i 0 */
            for(j=0; j<iNumTasks; j++){
                int d_ran = rand() % iNumTasks + 1;
                float w_ran = (float) (rand() % random2 + random1);
                myfile<<d_ran<<" "<<w_ran<<" "<<0<<endl;
            }
            myfile.close();
        }
    }

    cout<<endl<<"Generated succesfully. See files in "<<path;

    return 0;
}

