/*
 * Progam to generate file with x,y random coordinates for TRPTW.
 *
 * Arguments:
 *       <Num. of jobs> <range 1> <range 2> <path>
 *
 * Execution:
 *      5 1 4 /Users/yani/cc/research/code/travelling-repairman-problem/generate_trp_inst
 * Created by Yani on 10/19/15.
 *
 */

#include <iostream>
#include <fstream>
#include <string>
#include <stdio.h>
#include <time.h>
#include <cstdlib>

using namespace std;

int main(int argc, const char * argv[]) {
    /* initialize random seed: */
    srand (time(NULL));
    int iNumTasks = atoi(*(argv+1));
    int range1 = atoi(*(argv+2));
    int range2 = atoi(*(argv+3));
    string path = *(argv+4);

    float x=0.0,t=0.0;
    string name_file = "";
    string aux = "";
    int i=0, j=0;

    cout << "Number of jobs: " << iNumTasks<<endl;
    cout << "Range: "<<range1 << "-"<<range2 <<endl;
    aux = "";
    aux = path;
    aux += "/";
    aux += to_string(iNumTasks);
    aux += "_";
    aux += to_string(range1);
    aux += "_";
    aux += to_string(range2);
    aux +=".txt";
    ofstream myfile(aux.c_str());
    //myfile<<iNumTasks<<endl;
    for(i=0; i< iNumTasks; i++){
        x   = ( (float)rand( ) / ( (float)RAND_MAX + (float)range1 ) ) * range2;
        t   = ( (float)rand( ) / ( (float)RAND_MAX + (float)range1 ) ) * range2;
        myfile<<x<<" "<<t<<endl;
    }

    myfile.close();

    cout<<endl<<"Generated succesfully. See "<<aux;

    return 0;
}
