#!/bin/bash
dir=/Users/yani/CC/research/code/travelling-repairman-problem/gen_unit_task_instances/files
taskMin=3
taskMax=10
tests=50
randomMin=50
randomMax=100
log=0
clear
rm approximation.png
echo "Process started..."
echo "Generating instances..."
rm ../gen_unit_task_instances/files/tasks*
../gen_unit_task_instances/gen_unit_inst.o ${taskMin} ${taskMax} ${tests} ${dir} ${randomMin} ${randomMax}
echo "$'\n'Now executing greedy and exhaustive algorithms..."
result=$(./utask_greedy_ex.o 0 ${taskMin} ${taskMax} ${tests} ${dir} ${log})
path=`echo "${result}" | sed 's:/:\\\/:g'`
echo "Process ended."
cp gnuplot.txt gnuplot_command.txt
echo "Plotting in Gnuplot..."
sed -i.bak s/REPLACE/$path/g gnuplot_command.txt
gnuplot < gnuplot_command.txt
echo "Successfully ended. See plot file"
name="approximation"$taskMin"_"$taskMax"_"$tests"_r"$randomMin"_"$randomMax".png"
echo $name
cp approximation.png $name
open $name
