/*
 * Program that solves and compares a Greedy algorithm for Unit Time Scheduling Problem
 * from chapter 16 of CLRS with its exhaustive version.
 *
 * Parameters:
 *      mode: 0 for normal execution (mulitple files), 1 for only one file, int
 *      range_min: min number of tasks to generate, int
 *      range_max: max number of tasks to generate, int
 *      iTests: number of tests to generate, int
 *      path: directory path, string
 *      log: flag indicating log 0:no, 1:yes, int
 *
 * Execution:
 *      ./main.o mode range_min range_max iTests path log
 *
 * Notes:
 *      If mode is set to 1 then range_min, range_max and iTests may contain
 *      any value (cero) and <path> must be the full path of single input file.
 *
 * Yani. December 2014.
*/

#include <stdio.h>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <math.h>
#include <algorithm>

using namespace std;

int logg=0;
int iNumTasks=1;


/* Structs definition */
struct TASK{
    int ai;
    int di;
    float wi;
    static bool before_di( const TASK& c1, const TASK& c2 ) { return c1.di < c2.di; }
    static bool before_wi( const TASK& c1, const TASK& c2 ) { return c1.wi > c2.wi; }
    };

int *a_i;
int *d_i;
float *w_i;


/*
 * Function needed when calls to qsort function to order the task array by weight.
 * Module: Greedy version.
 * Parameters: const void *a, *b.
*/
int struct_cmp_by_w(const void *a, const void *b)
{
    struct TASK *ia = (struct TASK *)a;
    struct TASK *ib = (struct TASK *)b;
    return (int)(ib->wi - ia->wi);
    /* float comparison: returns negative if b > a
    and positive if a > b.*/
}

/*
 * Function that evaluates the independence idea of matroid.
 * Module: Greedy version.
 * Parameters: Vector containing task substructure
*/
int checkIndependence(vector<TASK> A){
    int isIndependent = 0;
    int i = 0, t = 0, num=0;
    t = A.at(0).di;

    // get the t value
    for(i=0;i<A.size();i++){
        if(A.at(i).di>t)
            t=A.at(i).di;
    }

    // check for independence
    for(i=0;i<A.size();i++){
        if(A.at(i).di<=t)
            num++;
    }

    if (num <= t)
        isIndependent = 1;
    else
        isIndependent = 0;
    return isIndependent;

}

/* Get the missed sum of wi. This is invoked by greedy algorithm.
 *  Module: Greedy version.
 *  Parameters:
 *
 *
 */
float missed_wi(vector<TASK> A){
    int i=0;
    float penalty = 0.0;
    for(i=0;i<A.size();i++){
        if ( (i+1) <= A.at(i).di) {
           // is in time
        }else{
           // is missed task
            penalty += A.at(i).wi;
        }
    }
    return penalty;
}


/*
 * Them main logic of greedy algorithm.
 * Module: Greedy version.
 * Parameters: array of tasks.
*/
vector<TASK> Greedy(TASK arrTasks[], float &measure_approx){
    int i = 0;

    /* aux array to manipulate in this function */
    // OLD: TASK *arrTasksAux =(TASK*)malloc(sizeof(TASK)*iNumTasks);
    TASK *arrTasksAux =new TASK[iNumTasks];

    for(i=0;i<iNumTasks;i++){
        arrTasksAux[i] = arrTasks[i];
    }

    vector<TASK> v_REJECTS;

    /*** step 1. A = 0 */
    vector<TASK> A;

    /*** step 2. Sorts w_i array in monotonically decreasing order by weight */

    //mergeSort(arrTasks, 0,iNumTasks-1); //first order in increasing order
    qsort(arrTasksAux, iNumTasks, sizeof(struct TASK), struct_cmp_by_w);

    // so far arrTasks is already ordered monotonically decreasing.

    /*** step 3. For each x in M.S taken monotonically decreasing order by weight */
    for(i=0; i<iNumTasks; i++){
        A.insert(A.end(),arrTasksAux[i]);
        /*** step 4. if A  union {x} \in M.I*/
        if( checkIndependence(A) ){
            // 1: is independent

            /*** step 5. A = A union {X}*/
            //  nothing to do since {x} is in subset

        } else{
            // 0: is not independent
            A.erase(A.end()-1);
            v_REJECTS.insert(v_REJECTS.end(),arrTasksAux[i]);
        }
    }

    /* aditional: order A vector by deadline increasing order */
    sort(A.begin(), A.end(), TASK::before_di);

    /* aditional: order rejected vector by deadline increasing order */
    sort(v_REJECTS.begin(), v_REJECTS.end(), TASK::before_wi);

    measure_approx = 0.0;
    /*COMMENTED
    for(i=0;i<v_REJECTS.size();i++){
        measure_approx += v_REJECTS.at(i).wi;
    }
    */

    A.insert(A.end(), v_REJECTS.begin(), v_REJECTS.end());

    measure_approx = missed_wi(A);
    if(logg==1) cerr<<" measure greedy: "<<measure_approx;

    delete[] arrTasksAux;

    /*** step 6. Return A. */
    return A;
}

/*Exhaustive Version functions */

/*
 * Function to get the factorial in order to know max number of schedules
 * Module: Exhaustive version.
 * Parameters: int number.
*/
unsigned int factorial(int number){
    int a = number, result = 1;
    if(number == 0 || number ==1) return 1;
    result = a;
    do{
        --a;
        result=result*a;
    }while(a>1);

    return result;
}

/*
 * The logic of Exhaustive version algorithm
 * Module: Exhaustive version.
 * Parameters: array of tasks, vector of vector of ints which store the optimal
 * schedules passed by reference.
*/
void Exhaustive(TASK arrTasks[], vector< vector<int> >& vResults, float& measure_opt){
    int fact = factorial(iNumTasks), i = 0, j=0;

    int *arrAux = new int[iNumTasks];

    for(i=0; i<iNumTasks; i++){
        arrAux[i] = arrTasks[i].ai;
    }

    /*** step 1. Creates a list to store all the schedules. */
    int **si = new int*[fact];
    for (i = 0; i < fact; i++) {
      si[i] = new int[iNumTasks];
    }

    /*** step 2. Permute all tasks****/
    for(j=0;j<iNumTasks;j++){
        *(*(si) + j) = arrAux[j];
    }
    for(i=1;i<fact && next_permutation(arrAux,arrAux+iNumTasks);i++){
        for(j=0;j<iNumTasks;j++){
            *(*(si+i) + j) = arrAux[j];
        }
    }


    /**** step 3. Evaluate scheduling ****/
    float *wi = new float[fact];
    for(i=0;i<fact;i++){
        *(wi+i)=0;
    }

    int flag=0; // to indicate if almost one task is missed
    float penalty = 0.0;
    for(i=0;i<fact;i++){
        penalty = 0.0;
        flag = 0;

        for(j=0;j<iNumTasks;j++){
            //cerr<<endl<<"Test: "<<arrTasks[ (*(*(si+i) + j)) - 1].di << ".wi: "<<arrTasks[(*(*(si+i) + j)) - 1 ].wi<<" <= "<<(j+1);
            //cerr<<" "<<*(*(si+i) + j); prints array of shedules.
           if ( (j+1) <= arrTasks[ (*(*(si+i) + j)) - 1].di ){
               // is in time
           }else{
               // is missed task
               penalty = penalty + arrTasks[ (*(*(si+i) + j)) - 1 ].wi;
               flag = 1;
           }
        }

        if (flag!=0){ // there are almost one missed task
            *(wi+i) = penalty;
            //*(flags+i)=1;
        }
        //cerr<<" "<<*(wi+i); //LOG
        //cerr<<" "<<*(flags+i); //LOG
    }

     /*** step 4. Get the min of sum of wi ****/
    float min=0.0;
    i=0;
    min = *(wi+i);
    int index_min = i;
    for (i=index_min; i < fact; i++) {
      if (*(wi+i) < min) {
          min =  *(wi+i);
          index_min = i;
      }
    }

    /*Get the measure opt */
    measure_opt = 0.0;
    measure_opt = min;


     /*** step 4. Check for more shedules and return those. */
    for (i=0; i < fact; i++) {
      if ( *(wi+i) == min) {
          index_min = i;

          /* insert a schedule in the vector of results */
          vector<int> schd;
          for(j=0;j<iNumTasks;j++){
              schd.push_back( *(*(si+index_min) + j) );
              // cerr<< *(*(si+index_min) + j)<< " ";
          }
          vResults.push_back(schd);
          // cerr <<*(wi+index_min)<<endl;
      }
    }


    /*** Free the virtual memory ***/
    delete wi;
    delete arrAux;
    for (i = 0; i < fact; i++) {
      delete *(si+i);
    }
    delete[] si;
}


/*
 Function that invoke greedy and exhaustive version.
*/

int solver_gred_exh(string file, float& per_rate)
{
    ifstream inFile;
    inFile.open(file.c_str());
    string line;
    int i=0, i1, i2;
    char string1[10];
    char string2[50];

    getline(inFile, line);
    while(line[0]!='p'){
        getline(inFile, line);
    }

    // Now p line is in buffer
    sscanf(line.c_str(), "%s %s %d", string1, string2, &iNumTasks);
    if(logg==1) cerr<<"Num. of tasks: "<<iNumTasks;

    a_i=new int[iNumTasks];
    d_i=new int[iNumTasks];
    w_i=new float[iNumTasks];

    getline(inFile, line);
    while(i<=iNumTasks){
        a_i[i] = (i+1);
        sscanf(line.c_str(), "%d %f %d", &d_i[i], &w_i[i], &i2);
        i++;
        getline(inFile, line);
    }

    //OLD TASK *arrTasks =(TASK*)malloc(sizeof(TASK)*iNumTasks);
    TASK *arrTasks = new TASK[iNumTasks];
    //another valid declaration: TASK arrTasks[iNumTasks];

    for(i=0; i<iNumTasks; i++){
        arrTasks[i].ai = a_i[i];
        arrTasks[i].di = d_i[i];
        arrTasks[i].wi = w_i[i];
    }

    vector<TASK> A;
    float measure_approx=0.0;
    A = Greedy(arrTasks, measure_approx);

    if(logg==1) cerr<<endl<<endl<<"Schedule by greedy algorithm with measure_approx=" << measure_approx<<":" <<endl;
    if(logg==1) for(i=0; i<A.size(); i++){
        cerr<<" "<<A.at(i).ai;
    }

    /* Now call exhaustive version */
    if(logg==1) cerr<<endl;


    vector< vector<int> > vSolutionEx;
    // vector iterator, not used
    vector <int>::iterator Iter;

    float measure_opt=0.0;

    Exhaustive(arrTasks, vSolutionEx, measure_opt);

    if(logg==1) cerr<< "Found "<<vSolutionEx.size()<< " optimal solutions with measure_opt=" <<measure_opt<<": "<<endl;

    /* Print vectors of exhaustive solutions */
    if(logg==1)
    for ( vector< vector<int> >::size_type i = 0; i < vSolutionEx.size(); i++ )
    {
       for ( std::vector<int>::size_type j = 0; j < vSolutionEx[i].size(); j++ )
       {
          std::cerr << vSolutionEx[i][j] << ' ';
       }
       std::cerr << std::endl;
    }

    /* Verify if greedy solution was found by exhaustive */
    if(logg==1){
        cerr<<"Now verifying solution."<<endl;
        vector<int> vSolutionGr;
        for(i=0; i<A.size(); i++){ //copy greedy solution to an array
            vSolutionGr.push_back(A.at(i).ai);
        }
        for ( vector< vector<int> >::size_type i = 0; i < vSolutionEx.size(); i++ )
        {
            if( equal(vSolutionGr.begin(), vSolutionGr.end(), vSolutionEx[i].begin()) ){
                cerr<<"Found!"<<endl;
            }
        }
     }

    /* Calculating approximation rate */

    per_rate=0.0;
    if(measure_opt == 0 && measure_approx != measure_opt){
        float total_wi = 0.0;
        for(int i=0;i<iNumTasks;i++){
            total_wi += arrTasks[i].wi;
        }
        per_rate = 1.0 + (measure_approx/total_wi);
        if(logg==1) cerr<<endl<<"s1 measure_opt is 0";
    }
    else{
        if(measure_opt==measure_approx){
            // mean that greedy sol measure is the same that optimal sol and they are not equal to 0
            per_rate = 1.0;
            if(logg==1) cerr<<endl<<"s2 measure are the same";
        }else{
            // all other cases when measure_approx is greather than measure_opt
            per_rate = measure_approx/measure_opt; //always it will be greater than 1.0
        }
    }

    if(logg==1) cerr<<endl<<"performance_rate: "<<per_rate;

    /*** Good practice. Free memory. ***/
    delete a_i;
    delete d_i;
    delete w_i;
    delete []arrTasks;

    return 0;
}

/*
 * The main logic.
 * See at top of this file to details of invocations.
*/
int main(int argc, char **argv)
{
    int mode = atoi(*(argv+1));
    int range_min = atoi(*(argv+2));
    int range_max = atoi(*(argv+3));
    int iTests = atoi(*(argv+4));
    string path_dir = *(argv+5);
    logg = atoi(*(argv+6));
    string file_name ="";
    string aux ="";
    int iNumTasks=0, i=0;

    float single_perf_rate = 0.0;
    float pi=0.0, vn=0.0;

    /* Output file to print pairs */
    string out_file = "";
    out_file = path_dir;
   /*get the time to name the output file:*/
    time_t now = time(NULL);
    tm * ptm = localtime(&now);
    char buffer[32];
    strftime(buffer, 32, "%H.%M.%S", ptm);

    out_file += "/0_out_";
    out_file += to_string(range_min);
    out_file += "-";
    out_file += to_string(range_max);
    out_file += "__";
    out_file += buffer ;
    out_file += ".txt";

    ofstream ofsOut(out_file.c_str());

    /* When mode=1 we only want to process a file and end program */
    if (mode == 1){
        file_name = path_dir;
        solver_gred_exh(file_name, single_perf_rate);
        ofsOut<<iNumTasks<<","<<single_perf_rate<<endl;
        ofsOut.close();
        cerr<<endl<<"Succesfully ended. See in "<<out_file<<endl;
        return 0;
    }

    for(iNumTasks=range_min; iNumTasks<=range_max; iNumTasks++){
        aux="";
        aux = path_dir+"/tasks";
        aux += to_string(iNumTasks);
        aux += "__";
        pi = 0.0;
        vn = 0.0;
        for(i=1;i<=iTests;i++){
            file_name = "";
            file_name = aux;
            file_name+=to_string(i);
            file_name+=".txt";
            single_perf_rate=0.0;
            if(logg==1) cerr<<endl<<"-----"<<endl<<file_name<<endl;
            solver_gred_exh(file_name, single_perf_rate);
            pi += single_perf_rate;
        }
        vn = pi/iTests; // get the average
        ofsOut<<iNumTasks<<" "<<vn<<endl;
    }
    ofsOut.close();
    cerr<<endl<<"Succesfully ended. See output in "<<out_file<<endl;
    cout<<out_file;

    return 0;
}
