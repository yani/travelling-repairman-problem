#!/bin/bash
path=/Users/yani/CC/research/code/travelling-repairman-problem/linear_prog_trp/
plot=1
segmentsRange1=50
segmentsRange2=1000
incrementSegments=50

gridRange1=50
gridRange2=50
incrementGrid=50

flagFile=0 # 0: normal, 1: custom by code
pathFile=/Users/yani/CC/research/code/travelling-repairman-problem/generate_trp_inst/5_1_4.txt

range1=1.0
range2=0
logCode=0
numTests=1
clear
clear
temp=0
rm *.dat
#example to run: java -jar ./dist/trp_4approx_java.jar 500 1.0 90.0 0
echo "------Inicio------">> FILE_out1.txt

for i in $(seq $segmentsRange1 $incrementSegments $segmentsRange2)  #segments
do
	numSegments=$i

	for j in $(seq $gridRange1 $incrementGrid $gridRange2)   #grill size
	do
		timeAvg=0.0

		for k in $(seq 1 1 $numTests)   #tests and avg
		do
			sizeGrid=$j
			nameFile=graphic_$numSegments"_"$sizeGrid".ps"
		    echo "Processing $numSegments and $sizeGrid vertex grill"    
		    temp=`echo "$sizeGrid - 2" | bc -l`
		    range2=`echo "0.707106 * $temp" | bc -l`		
			#java -jar ./dist/trp_4approx_java.jar ${numSegments} ${range1} ${range2} ${logCode} ${flagFile} ${pathFile} >> FILE_out.txt		
			java -jar ./dist/trp_4approx_java.jar ${numSegments} ${range1} ${range2} ${logCode} ${flagFile} ${pathFile}	>> FILE_out1.txt
			if [ $plot = 1 ]; then
				echo "Gurobi ended. Plotting in Gnuplot..."
				gnuplot < gnuplot4trp.txt
				gnuplot < gnuplot8trp.txt
				#echo "Successfully ended. See $nameFile file"
				#cp out.ps $nameFile
				open out4approx.ps
				open out8approx.ps
			fi
			echo "End."
		done
		 
	done
done
echo "------Fin-------">> FILE_out1.txt
