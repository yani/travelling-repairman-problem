
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import static java.lang.Math.cos;
import static java.lang.Math.sin;
import java.util.ArrayList;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *  The Implementation of Traveling Repairman Problem
 * 
 * @author yani
 */

/**
 * The Implementation of Traveling Repairman Problem
 * @author yani
 */
public class Solve8Approx {
    public static final float SQUARE_SIZE = (float) 0.707106;
    public static final float PI = (float) 3.14159265;
    
    public static void create_instance(TrpProblem instances[], int N, float range1, float range2) {
        int i=0;
        instances[0] = new TrpProblem();
        instances[0].x = (float) 0.2;
        instances[0].t=(float) 0.4;

        instances[1] = new TrpProblem();
        instances[1].x = (float) 0.43449;
        instances[1].t = (float) 0.48693;

        instances[2] = new TrpProblem();
        instances[2].x=(float) 0.868971;
        instances[2].t=(float) 0.799383;

        instances[3] = new TrpProblem();
        instances[3].x=(float) 1.18971;
        instances[3].t=(float) 0.999383;
        
        System.out.println("Instances generated: ");
        for(i=0; i<N; i++){
            //instances[i].x   = ( (float)rand( ) / ( (float)RAND_MAX + (float)range1 ) ) * range2;
            //instances[i].t   = ( (float)rand( ) / ( (float)RAND_MAX + (float)range1 ) ) * range2;
            System.out.println(instances[i].x + " " + instances[i].t);

            instances[i].x_unit=(float) (instances[i].x + cos(45 * PI / 180.0));
            instances[i].t_unit=(float) (instances[i].t + sin(45 * PI / 180.0));
            System.out.println(instances[i].x_unit+" "+instances[i].t_unit);

            instances[i].p_i=0;
            instances[i].r_i=i/2;
            instances[i].d=i;
            instances[i].t_i=i;
        }
        
    }

    /*
    * Check if two line segments intersects. Return 1 if collides
    *
    *
    */
    public static int intersect(Point a,Point b,Point c,Point d)
    {
       float den = ((d.y-c.y)*(b.x-a.x)-(d.x-c.x)*(b.y-a.y));
       float num1 = ((d.x - c.x)*(a.y-c.y) - (d.y- c.y)*(a.x-c.x));
       float num2 = ((b.x-a.x)*(a.y-c.y)-(b.y-a.y)*(a.x-c.x));
       float u1 = num1/den;
       float u2 = num2/den;
       //std::cout << u1 << ":" << u2 << std::endl;
       if (den == 0 && num1  == 0 && num2 == 0)
           /* The two lines are coincidents */
           return 3;
       if (den == 0)
           /* The two lines are parallel */
           return 2;
       if (u1 <0 || u1 > 1 || u2 < 0 || u2 > 1)
           /* Lines do not collide */
           return 0;
       /* Lines DO collide */
       return 1;
    }
    
    public static int getMaxNumVertexSide(TrpProblem instances[], int N){
        int i=0;
        float maxy=0, maxx=0;
        int vertex_side=1;  // start in 1 since vertex in 0,0 also is counted
        float cover_area=(float) 0.0;

        /* get the max value of the y and x axis */
        maxy=instances[0].t_unit;
        maxx=instances[0].x_unit;
        for(i=0;i<N;i++){
            if(instances[i].t_unit>maxy)
                maxy=instances[i].t_unit;
            if(instances[i].x_unit>maxx)
                maxx=instances[i].x_unit;
        }
        /* take the max value of the axis */
        if(maxy > maxx)
            maxx=maxy;

        /* now maxx has the max value of both axis.
         * We calculate then the number of vertex needed */

        do{
            cover_area+=SQUARE_SIZE;
            vertex_side++;
        }while(cover_area<maxx);

        return vertex_side;
    }

    /* 
        Invocation with parameters: tinyEWDAG.txt 5
    */
    static void invoke_longest_path(String[] args){
        In in = new In(args[0]);
        int s = Integer.parseInt(args[1]);
        EdgeWeightedDigraph G = new EdgeWeightedDigraph(in);

        AcyclicLP lp = new AcyclicLP(G, s);

        for (int v = 0; v < G.V(); v++) {
            if (lp.hasPathTo(v)) {
                StdOut.printf("%d to %d (%.2f)  ", s, v, lp.distTo(v));
                for (DirectedEdge e : lp.pathTo(v)) {
                    StdOut.print(e + "   ");
                }
                StdOut.println();
            }
            else {
                StdOut.printf("%d to %d         no path\n", s, v);
            }
        }
        
        System.out.println("\nThe best route is: ");
        for (DirectedEdge e : lp.pathTo(G.V()-1)) {
                    StdOut.print(e + "   ");
        }
        System.out.println("");
        
    }
    static void write_file4lp(int num_vertex, int num_edges){
        try {
  
            File file = new File("DAG.txt");

            // if file doesnt exists, then create it
            if (!file.exists()) {
                    file.createNewFile();
            }
            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(String.valueOf(num_vertex));
            bw.write("\n");
            bw.write(String.valueOf(num_edges));
            bw.write("\n");
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    static void add_line_file4lp(String line){        
        try {
  
            File file = new File("DAG.txt");

            // if file doesnt exists, then create it
            if (!file.exists()) {
                    file.createNewFile();
            }
            FileWriter fw = new FileWriter(file.getAbsoluteFile(), true);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(line);
            bw.write("\n");
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
     /**
     * Creates file to plot segments with Gnuplot.
     * @param Instances array of TrpProblem
     * @return NA
     */    
    static void create_file_segments(TrpProblem[] instances, int n){
        
        int i=0, N=n;
        
        try {
  
            File file = new File("segments.dat");

            // if file doesnt exists, then create it
            if (!file.exists()) {
                    file.createNewFile();
            }
            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);
            
            for(i=0; i<N; i++){
                bw.write(instances[i].x +" "+ instances[i].t + "\n");
                bw.write(instances[i].x_unit +" "+ instances[i].t_unit + "\n");
                bw.write("\n");
            }
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        
    }
    
    
    static void Solve8approx(String[] args){
        int N = Integer.valueOf(args[0]);
        float range1=Float.valueOf(args[1]);
        float range2=Float.valueOf(args[2]);

        int i=0, num_vertices=0, j=0, k=0, count_intersections=0, vertex_side=0, num_edges=0;
        TrpProblem[] instances = new TrpProblem[N];
        
        create_instance(instances, N, range1, range2);

        /* calculates the number of vertex side needed for the grid */
        vertex_side = getMaxNumVertexSide(instances, N);
        num_vertices = (vertex_side)*(vertex_side);
        
        System.out.println("Num. segmentos: "+N);
        System.out.println("Grafo tendrá "+num_vertices+" vertices");

        /* Create the grid and set its coordinates x,y */
        Grid[][] points_grid = new Grid[vertex_side][vertex_side];
        
        for(j=0; j<vertex_side; j++){
            for(i=0; i<vertex_side; i++){
                points_grid[j][i] = new Grid();
                points_grid[j][i].x = SQUARE_SIZE*i;
                points_grid[j][i].y = SQUARE_SIZE*j;
            }
        }

        for(j=0; j<vertex_side; j++){
            for(i=0; i<vertex_side; i++){
                System.out.print(" v"+(i+(j*vertex_side))+":("+points_grid[j][i].x +","+ points_grid[j][i].y+");\t");
            }
            System.out.println();
          
        }

        // Create file for create graph
        num_edges = 2*vertex_side*(vertex_side-1);
        write_file4lp(num_vertices, num_edges);
        String line;
        
        /* calculates the weight of each horizontal and vertical gridline */
        Point p1 = new Point(0, 0);
        Point q1 = new Point(0, 0);
        Point p2 = new Point(0, 0);
        Point q2 = new Point(0, 0);
        int current, target, index_edge=0;
        
        List<Integer> vector_edges = new ArrayList<>();

        for(j=0; j<vertex_side; j++){
            for(i=0; i<vertex_side; i++){
                
                current = i+(j*vertex_side);

                /** start checking horizontal intersection **/
                if(i < vertex_side-1){

                    p1.x=points_grid[j][i].x; p1.y=points_grid[j][i].y;
                    q1.x=points_grid[j][i+1].x; q1.y=points_grid[j][i+1].y;  //check intersections with horizontal lines
                    count_intersections=0;
                    for(k=0;k<N;k++){
                        p2.x= instances[k].x; p2.y =instances[k].t;
                        q2.x= instances[k].x_unit; q2.y = instances[k].t_unit;
                        if( intersect(p1, q1, p2, q2) == 1){
                            count_intersections++;
                        }
                    }                
                    target = i+(j*vertex_side) + 1;
                    
                    line = "";
                    line = current + " " + target + " " + count_intersections; 
                    add_line_file4lp(line);
                    index_edge++;
                  
                    

                }

                /** checks vertical intersection **/
                if(j < vertex_side-1){
                    p1.x = points_grid[j][i].x; p1.y=points_grid[j][i].y;
                    q1.x = points_grid[j+1][i].x; q1.y=points_grid[j+1][i].y;
                    count_intersections=0;
                    for(k=0;k<N;k++){
                        p2.x= instances[k].x; p2.y =instances[k].t;
                        q2.x= instances[k].x_unit; q2.y = instances[k].t_unit;
                        if( intersect(p1, q1, p2, q2) == 1 ){
                            count_intersections++;
                        }
                    }
                    target = i+(j*vertex_side) + vertex_side;

                    line = "";
                    line = current + " " + target + " " + count_intersections;  
                    add_line_file4lp(line);
                    index_edge++;
                }
            }
        }
        
        /* Invokes to longes path function */
        System.out.println("Invoking to Longest_Path...");
        String[] args_lp = new String[2];
        args_lp[0]= "DAG.txt";
        args_lp[1]= "0";
        invoke_longest_path(args_lp);
        
        /*create file to plot segments */
        create_file_segments(instances, N);

    }
    
    static void Solve4approx(String[] args){
        int N = Integer.valueOf(args[0]);
        float range1=Float.valueOf(args[1]);
        float range2=Float.valueOf(args[2]);

        int i=0, num_vertices=0, j=0, k=0, count_intersections=0, vertex_side=0, num_edges=0;
        TrpProblem[] instances = new TrpProblem[N];
        
        create_instance(instances, N, range1, range2);

        /* calculates the number of vertex side needed for the grid */
        vertex_side = getMaxNumVertexSide(instances, N);
        num_vertices = (vertex_side)*(vertex_side);
        
        System.out.println("Num. segmentos: "+N);
        System.out.println("Grafo tendrá "+num_vertices+" vertices");

        /* Create the grid and set its coordinates x,y */
        Grid[][] points_grid = new Grid[vertex_side][vertex_side];
        
        for(j=0; j<vertex_side; j++){
            for(i=0; i<vertex_side; i++){
                points_grid[j][i] = new Grid();
                points_grid[j][i].x = SQUARE_SIZE*i;
                points_grid[j][i].y = SQUARE_SIZE*j;
            }
        }

        for(j=0; j<vertex_side; j++){
            for(i=0; i<vertex_side; i++){
                System.out.print(" v"+(i+(j*vertex_side))+":("+points_grid[j][i].x +","+ points_grid[j][i].y+");\t");
            }
            System.out.println();
          
        }

        // Create file for create graph
        num_edges = 2*vertex_side*(vertex_side-1);
        write_file4lp(num_vertices, num_edges);
        String line;
        
        /* calculates the weight of each horizontal and vertical gridline */
        Point p1 = new Point(0, 0);
        Point q1 = new Point(0, 0);
        Point p2 = new Point(0, 0);
        Point q2 = new Point(0, 0);
        int current, target, index_edge=0;
        
        List<Integer> vector_edges = new ArrayList<>();

        for(j=0; j<vertex_side; j++){
            for(i=0; i<vertex_side; i++){
                
                current = i+(j*vertex_side);

                /** start checking horizontal intersection **/
                if(i < vertex_side-1){

                    p1.x=points_grid[j][i].x; p1.y=points_grid[j][i].y;
                    q1.x=points_grid[j][i+1].x; q1.y=points_grid[j][i+1].y;  //check intersections with horizontal lines
                    count_intersections=0;
                    for(k=0;k<N;k++){
                        p2.x= instances[k].x; p2.y =instances[k].t;
                        q2.x= instances[k].x_unit; q2.y = instances[k].t_unit;
                        if( intersect(p1, q1, p2, q2) == 1){
                            count_intersections++;
                        }
                    }                
                    target = i+(j*vertex_side) + 1;
                    
                    line = "";
                    line = current + " " + target + " " + count_intersections; 
                    add_line_file4lp(line);
                    index_edge++;
                  
                    

                }

                /** checks vertical intersection **/
                if(j < vertex_side-1){
                    p1.x = points_grid[j][i].x; p1.y=points_grid[j][i].y;
                    q1.x = points_grid[j+1][i].x; q1.y=points_grid[j+1][i].y;
                    count_intersections=0;
                    for(k=0;k<N;k++){
                        p2.x= instances[k].x; p2.y =instances[k].t;
                        q2.x= instances[k].x_unit; q2.y = instances[k].t_unit;
                        if( intersect(p1, q1, p2, q2) == 1 ){
                            count_intersections++;
                        }
                    }
                    target = i+(j*vertex_side) + vertex_side;

                    line = "";
                    line = current + " " + target + " " + count_intersections;  
                    add_line_file4lp(line);
                    index_edge++;
                }
            }
        }
        
        /* Invokes to longes path function */
        System.out.println("Invoking to Longest_Path...");
        String[] args_lp = new String[2];
        args_lp[0]= "DAG.txt";
        args_lp[1]= "0";
        invoke_longest_path(args_lp);
        
        /*create file to plot segments */
        create_file_segments(instances, N);

    }
    /**
     * Unit tests the <tt>AcyclicLP</tt> data type.
     */
    public static void main(String[] args) {
        
        Solve8approx(args);
    }
}
