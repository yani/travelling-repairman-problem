
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import static java.lang.Math.cos;
import static java.lang.Math.sin;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *  The Implementation of Traveling Repairman Problem
 * 
 *  flagFile:
 *  Arguments: ${numSegments} ${range1} ${range2} ${logCode} ${flagFile} ${pathFile}
 *      flagFile: 0 for nothing, 1 for specify a File
 *      pathFile: optional, full path of file. Only required if flagFile is equal to 1
 * 
 * Execution:
 *      With input file:
 *           java -jar ./dist/trp_4approx_java.jar 5 0 0 0 1 /Users/yani/CC/research/code/travelling-repairman-problem/generate_trp_inst/5_1_4.txt
 * @author yani
 */

/**
 * The Implementation of Traveling Repairman Problem
 * @author yani
 */
public class SolveTRP {
    public static final float SQUARE_SIZE = (float) 0.707106;
    public static final float PI = (float) 3.14159265;
    static Double bestObjFun = 0.0;
    static int LOG=1;
    
    
    public static void create_instance(TrpProblem instances[], int N, float range1, float range2, int flagGeneration) {
        int i=0;
        double randNumber;
        Random r = new Random();
        
        float auxRange = range2 - range1 - 1;
        /* FOR INSTANCES WITH SPECIAL CASES*/
        /*
        instances[0] = new TrpProblem();
        instances[0].x = (float) .9;
        instances[0].t=(float) 1.35;

        
        instances[1] = new TrpProblem();
        instances[1].x = (float) 0.3;
        instances[1].t = (float) 0.6;

        instances[2] = new TrpProblem();
        instances[2].x=(float) 1.1;
        instances[2].t=(float) 2.6;
        
        instances[3] = new TrpProblem();
        instances[3].x = (float) 1.5;
        instances[3].t=(float) .6;  // SPECIAL CASE
        //instances[3].t=(float) .2;  // NORMAL CASE

        instances[4] = new TrpProblem();
        instances[4].x = (float) .9;
        instances[4].t = (float) .3;

        instances[5] = new TrpProblem();
        instances[5].x=(float) 0.2;
        instances[5].t=(float) 1.7;
        
        instances[6] = new TrpProblem();
        instances[6].x = (float) 1.7;
        instances[6].t=(float) 1.2;

        
        instances[7] = new TrpProblem();
        instances[7].x = (float) 1.5;
        instances[7].t = (float) 2.5;
        
        System.out.println("Instances generated: ");
        */
        for(i=0; i<N; i++){
            // COMMENT next three lines to test.
            instances[i] = new TrpProblem();            
            instances[i].x   = (float) (r.nextFloat() * auxRange + range1);
            instances[i].t   = (float) (r.nextFloat() * auxRange + range1);
            
            if(LOG==1) System.out.println(instances[i].x + " " + instances[i].t);

            instances[i].x_unit=(float) (instances[i].x + cos(45 * PI / 180.0));
            instances[i].t_unit=(float) (instances[i].t + sin(45 * PI / 180.0));
            if(LOG==1) System.out.println(instances[i].x_unit+" "+instances[i].t_unit);

            instances[i].p_i=0;
            instances[i].r_i=i/2;
            instances[i].d=i;
            instances[i].t_i=i;
        }
        
    }

    public static void readFile(TrpProblem instances[], int N, String pathFile){
        int i=0;
        BufferedReader br = null;
    
        String currentLine = "";       
        try {
            br = new BufferedReader(new FileReader(pathFile));
            
            while ((currentLine = br.readLine()) != null) {
                instances[i] = new TrpProblem();
                instances[i].x = Float.valueOf(currentLine.split(" ")[0]);
                instances[i].t = Float.valueOf(currentLine.split(" ")[1]);  
                
                // the other attributes remains same.
                instances[i].x_unit=(float) (instances[i].x + cos(45 * PI / 180.0));
                instances[i].t_unit=(float) (instances[i].t + sin(45 * PI / 180.0));
                if(LOG==1) System.out.println(instances[i].x_unit+" "+instances[i].t_unit);

                instances[i].p_i=0;
                instances[i].r_i=i/2;
                instances[i].d=i;
                instances[i].t_i=i;
                
                i++;
            }  
            br.close();
        }catch (IOException ex) {
                Logger.getLogger(SolveTRP.class.getName()).log(Level.SEVERE, null, ex);
        } 
        
        
    }
            
    /*
    * Check if two line segments intersects. Return 1 if collides
    *
    *
    */
    public static int intersect(Point a,Point b,Point c,Point d)
    {
       float den = ((d.y-c.y)*(b.x-a.x)-(d.x-c.x)*(b.y-a.y));
       float num1 = ((d.x - c.x)*(a.y-c.y) - (d.y- c.y)*(a.x-c.x));
       float num2 = ((b.x-a.x)*(a.y-c.y)-(b.y-a.y)*(a.x-c.x));
       float u1 = num1/den;
       float u2 = num2/den;
       //std::cout << u1 << ":" << u2 << std::endl;
       if (den == 0 && num1  == 0 && num2 == 0)
           /* The two lines are coincidents */
           return 3;
       if (den == 0)
           /* The two lines are parallel */
           return 2;
       if (u1 <0 || u1 > 1 || u2 < 0 || u2 > 1)
           /* Lines do not collide */
           return 0;
       /* Lines DO collide */
       return 1;
    }
    
    public static int getMaxNumVertexSide(TrpProblem instances[], int N){
        int i=0;
        float maxy=0, maxx=0;
        int vertex_side=1;  // start in 1 since vertex in 0,0 also is counted
        float cover_area=(float) 0.0;

        /* get the max value of the y and x axis */
        maxy=instances[0].t_unit;
        maxx=instances[0].x_unit;
        for(i=0;i<N;i++){
            if(instances[i].t_unit>maxy)
                maxy=instances[i].t_unit;
            if(instances[i].x_unit>maxx)
                maxx=instances[i].x_unit;
        }
        /* take the max value of the axis */
        if(maxy > maxx)
            maxx=maxy;

        /* now maxx has the max value of both axis.
         * We calculate then the number of vertex needed */

        do{
            cover_area+=SQUARE_SIZE;
            vertex_side++;
        }while(cover_area<maxx);

        return vertex_side;
    }

    /* 
        Invocation with parameters: tinyEWDAG.txt 5
        args[0] = tinyEWDAG.txt
        args[1] = 5
        
    */
    static void invoke_longest_path(String[] args, List<Integer> bestRoute, int type, Grid[][] ext_new_grid, int ext_vertex_side_new){
        In in = new In(args[0]);
        int s = Integer.parseInt(args[1]);
        EdgeWeightedDigraph G = new EdgeWeightedDigraph(in);             
        AcyclicLP lp = new AcyclicLP(G, s);
        
        if(LOG==1){ // prints all the paths found by the algorithm
            for (int v = 0; v < G.V(); v++) {
                if (lp.hasPathTo(v)) {
                    StdOut.printf("%d to %d (%.2f)  ", s, v, lp.distTo(v));
                    for (DirectedEdge e : lp.pathTo(v)) {
                        StdOut.print(e + "   ");
                    }
                    StdOut.println();
                }
                else {
                    StdOut.printf("%d to %d         no path\n", s, v);
                }
            }
        }
        long startTime = System.currentTimeMillis();  
        if(LOG==1)System.out.println("\nThe best route is: "+lp.distTo(G.V()-1));
        // TEMP ZAR bestObjFun = lp.distTo(G.V()-1);
        // solo para probar que el tipo sea la 8 aprox: IMPORTANTE QUITAR
        if (type==1)
            bestObjFun = lp.distTo(G.V()-1);
        for (DirectedEdge e : lp.pathTo(G.V()-1)) {
                    if(LOG==1) StdOut.print(e.from() + "   ");
                    bestRoute.add(e.from());   // puts the vertex in the route list
        }
        //System.out.println("");
        long estimatedTime = System.currentTimeMillis() - startTime;
        if(LOG==1)System.out.println("\nTiempo: "+ ((float)estimatedTime/1000.0) );
        
        /* creates structure for graph trajectories */
        if(type==0){
            // is 8 approximation mode
            create_file_segments_dag(ext_new_grid, ext_vertex_side_new, G, "graph8ap.dat");
        }
        else{
            // is 4 approximation mode
            create_file_segments_dag(ext_new_grid, ext_vertex_side_new, G, "graph4ap.dat");   // graph segments
        }
        
        
        
    }
    static void write_file4lp(int num_vertex, int num_edges){
        try {
  
            File file = new File("DAG.txt");

            // if file doesnt exists, then create it
            if (!file.exists()) {
                    file.createNewFile();
            }
            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(String.valueOf(num_vertex));
            bw.write("\n");
            bw.write(String.valueOf(num_edges));
            bw.write("\n");
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    static void add_line_file4lp(String line){        
        try {
  
            File file = new File("DAG.txt");

            // if file doesnt exists, then create it
            if (!file.exists()) {
                    file.createNewFile();
            }
            FileWriter fw = new FileWriter(file.getAbsoluteFile(), true);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(line);
            bw.write("\n");
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    static void add_list_file4lp(List<String> list_edges){        
        try {
  
            File file = new File("DAG.txt");

            // if file doesnt exists, then create it
            if (!file.exists()) {
                    file.createNewFile();
            }
            FileWriter fw = new FileWriter(file.getAbsoluteFile(), true);
            BufferedWriter bw = new BufferedWriter(fw);
            int i=0;
            for(i=0;i<list_edges.size();i++){
                bw.write(list_edges.get(i));
                bw.write("\n");
            }
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
     /**
     * Creates file to plot segments with Gnuplot.
     * @param Instances array of TrpProblem
     * @return NA
     */    
    static void create_file_segments(TrpProblem[] instances, int n){
        int i=0, N=n;
        
        try {
  
            File file = new File("segments.dat");

            // if file doesnt exists, then create it
            if (!file.exists()) {
                    file.createNewFile();
            }
            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);
            
            for(i=0; i<N; i++){
                bw.write(instances[i].x +" "+ instances[i].t + "\n");
                bw.write(instances[i].x_unit +" "+ instances[i].t_unit + "\n");
                bw.write("\n");
            }
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        } 
    }
    
         /**
     * Creates file to plot DAG vertex segments with Gnuplot.
     * @param Instances array of TrpProblem
     * @return NA
     */    
    static void create_file_segments_dag(Grid[][] newgrid, int n, EdgeWeightedDigraph G, String filename){
        int i=0,j=0, N=n, vertex;
        
        try {
  
            File file = new File(filename);

            // if file doesnt exists, then create it
            if (!file.exists()) {
                    file.createNewFile();
            }
            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);
            
            
            for (DirectedEdge e : G.edges()) {
                vertex = e.from();
                for (j=0;j<N;j++){
                   for(i=0; i<N; i++){
                     if(newgrid[j][i]!=null){
                         if(newgrid[j][i].v == vertex){
                            bw.write(newgrid[j][i].x +" "+ newgrid[j][i].y + "\n");
                         }
                     }
                   }
                }
                
                // Get the target vertex from our grid:
                vertex = e.to();
                for (j=0;j<N;j++){
                   for(i=0; i<N; i++){
                     if(newgrid[j][i]!=null){
                         if(newgrid[j][i].v == vertex){
                            bw.write(newgrid[j][i].x +" "+ newgrid[j][i].y + "\n");
                         }
                     }
                   }
                 }
                 bw.write("\n");
               
            }
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        } 
    }
    
        /**
     * Creates file to plot trajectories with Gnuplot.
     * @param Trajectories
     * @return NA
     */    
    static void create_file_trajectory(Grid[][] newgrid, int n, List<Integer> bestRoute, String filename){
        int i=0,j=0, N=n, iv;
        
        try {
  
            File file = new File(filename);

            // if file doesnt exists, then create it
            if (!file.exists()) {
                    file.createNewFile();
            }
            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);
            
            for (j=0;j<N;j++){
                for(i=0; i<N; i++){
                    if(newgrid[j][i]!=null){
                        //searches the vector elements
                        for(iv=0; iv<bestRoute.size(); iv++){
                            if (newgrid[j][i].v == bestRoute.get(iv)){
                                bw.write(newgrid[j][i].x +" "+ newgrid[j][i].y + "\n");
                            }
                        }       
                    }
                }
            }
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        } 
    }
    
    /* function to measure real captured8 segments */
    static void getNumCaptured8a (TrpProblem[] instances, int numInstances, Grid[][] points_grid, List<Integer> bestRoute, int vertex_side){
        bestObjFun=0.0;
        int i=0, j=0, k=0, h=0;
        int v1, v2;
        Point p1 = new Point(0, 0);
        Point q1 = new Point(0, 0);
        Point p2 = new Point(0, 0);
        Point q2 = new Point(0, 0);
        
        while (i < (bestRoute.size()-1) )   {
                v1=bestRoute.get(i);
                v2=bestRoute.get(i+1);
                
                // Get the points of segments route:
                for(j=0; j<vertex_side;j++){
                    for(k=0; k<vertex_side;k++){
                        if(points_grid[j][k] == null){
                            continue;
                        }
                        if(points_grid[j][k].v == v1 || points_grid[j][k].v == v2){
                            if(points_grid[j][k].v == v1){
                                p1.x = points_grid[j][k].x;
                                p1.y = points_grid[j][k].y;
                            }
                            if(points_grid[j][k].v == v2){
                                q1.x = points_grid[j][k].x;
                                q1.y = points_grid[j][k].y;             
                            }
                        }
                    }
                }
            
                // Get the segments  of the instances               
                for(h=0; h<numInstances;h++){
                    if (instances[h].captured8==0){
                        p2.x = instances[h].x;
                        p2.y = instances[h].t;
                        q2.x = instances[h].x_unit;
                        q2.y = instances[h].t_unit;
                        
                        if(intersect(p1, q1, p2, q2)==1){
                            instances[h].captured8=1;
                            bestObjFun++;
                        }
                    }
                }           
                i++;
        }  
    }    
    
    
    /* function to measure real captured4 segments */
    static void getNumCaptured4a (TrpProblem[] instances, int numInstances, Grid[][] points_grid, List<Integer> bestRoute, int vertex_side){
        bestObjFun=0.0;
        int i=0, j=0, k=0, h=0;
        int v1, v2;
        Point p1 = new Point(0, 0);
        Point q1 = new Point(0, 0);
        Point p2 = new Point(0, 0);
        Point q2 = new Point(0, 0);
        
        while (i < (bestRoute.size()-1) )   {
                v1=bestRoute.get(i);
                v2=bestRoute.get(i+1);
                
                // Get the points of segments route:
                for(j=0; j<vertex_side;j++){
                    for(k=0; k<vertex_side;k++){
                        if(points_grid[j][k] == null){
                            continue;
                        }
                        if(points_grid[j][k].v == v1 || points_grid[j][k].v == v2){
                            if(points_grid[j][k].v == v1){
                                p1.x = points_grid[j][k].x;
                                p1.y = points_grid[j][k].y;
                            }
                            if(points_grid[j][k].v == v2){
                                q1.x = points_grid[j][k].x;
                                q1.y = points_grid[j][k].y;             
                            }
                        }
                    }
                }
                
                
                // Get the segments  of the instances
                
                for(h=0; h<numInstances;h++){
                    if (instances[h].captured4==0){
                        p2.x = instances[h].x;
                        p2.y = instances[h].t;
                        q2.x = instances[h].x_unit;
                        q2.y = instances[h].t_unit;
                        
                        if(intersect(p1, q1, p2, q2)==1){
                            instances[h].captured4=1;
                            bestObjFun++;
                        }
                    }
                }
                
                
                i++;
        }
    
        
        
    }    
    
    /* args [0], [1] and [2] were used in previous method */
    static void Solve8approx(String[] args, TrpProblem[] instances){
        int N = Integer.valueOf(args[0]);        

        int i=0, num_vertices=0, j=0, k=0, count_intersections=0, vertex_side=0, num_edges=0;
                
        //create_instance(instances, N, range1, range2);

        /* calculates the number of vertex side needed for the grid */
        vertex_side = getMaxNumVertexSide(instances, N);
        num_vertices = (vertex_side)*(vertex_side);
        
        if(LOG==1) System.out.println("Num. segmentos: "+N);
        if(LOG==1) System.out.println("Grafo tendrá "+num_vertices+" vertices");

        /* Create the grid and set its coordinates x,y */
        Grid[][] points_grid = new Grid[vertex_side][vertex_side];
        
        for(j=0; j<vertex_side; j++){
            for(i=0; i<vertex_side; i++){
                points_grid[j][i] = new Grid();
                points_grid[j][i].x = SQUARE_SIZE*i;
                points_grid[j][i].y = SQUARE_SIZE*j;
                points_grid[j][i].v = (i+(j*vertex_side)); // ADDED
            }
        }

        if(LOG==1){   // prints the initial grid for the graph
            for(j=0; j<vertex_side; j++){
                for(i=0; i<vertex_side; i++){
                    System.out.print(" v"+(i+(j*vertex_side))+":("+points_grid[j][i].x +","+ points_grid[j][i].y+");\t");
                }
                System.out.println();

            }
        }

        // Create file for create graph
        num_edges = 2*vertex_side*(vertex_side-1);
        write_file4lp(num_vertices, num_edges);
        String line;
        
        /* calculates the weight of each horizontal and vertical gridline */
        Point p1 = new Point(0, 0);
        Point q1 = new Point(0, 0);
        Point p2 = new Point(0, 0);
        Point q2 = new Point(0, 0);
        int current, target, index_edge=0;
        
        List<Integer> vector_edges = new ArrayList<>();

        for(j=0; j<vertex_side; j++){
            for(i=0; i<vertex_side; i++){
                
                current = i+(j*vertex_side);

                /** start checking horizontal intersection **/
                if(i < vertex_side-1){

                    p1.x=points_grid[j][i].x; p1.y=points_grid[j][i].y;
                    q1.x=points_grid[j][i+1].x; q1.y=points_grid[j][i+1].y;  //check intersections with horizontal lines
                    count_intersections=0;
                    for(k=0;k<N;k++){
                        p2.x= instances[k].x; p2.y =instances[k].t;
                        q2.x= instances[k].x_unit; q2.y = instances[k].t_unit;
                        if( intersect(p1, q1, p2, q2) == 1){
                            count_intersections++;
                        }
                    }                
                    target = i+(j*vertex_side) + 1;
                    
                    line = "";
                    line = current + " " + target + " " + count_intersections; 
                    add_line_file4lp(line);
                    index_edge++;
                  
                    

                }

                /** checks vertical intersection **/
                if(j < vertex_side-1){
                    p1.x = points_grid[j][i].x; p1.y=points_grid[j][i].y;
                    q1.x = points_grid[j+1][i].x; q1.y=points_grid[j+1][i].y;
                    count_intersections=0;
                    for(k=0;k<N;k++){
                        p2.x= instances[k].x; p2.y =instances[k].t;
                        q2.x= instances[k].x_unit; q2.y = instances[k].t_unit;
                        if( intersect(p1, q1, p2, q2) == 1 ){
                            count_intersections++;
                        }
                    }
                    target = i+(j*vertex_side) + vertex_side;

                    line = "";
                    line = current + " " + target + " " + count_intersections;  
                    add_line_file4lp(line);
                    index_edge++;
                }
            }
        }
        
        /* Invokes to longes path function */
        System.out.println("Invoking to Longest_Path...");
        String[] args_lp = new String[2];
        args_lp[0]= "DAG.txt";
        args_lp[1]= "0";
        
        //used to store the best route
        List<Integer> bestRoute = new ArrayList<Integer>(); 
        
        // third parameter is 0 when is 8approx, or 1 when is 4approx
        invoke_longest_path(args_lp, bestRoute, 0, points_grid, vertex_side);
        
        //get captured8 segments
        getNumCaptured8a(instances, N, points_grid, bestRoute, vertex_side);
        //getNumCaptured(instances, N, points_grid, bestRoute, vertex_side);
        
        /*create file to plot segments */
        create_file_segments(instances, N); // instances segments 
        create_file_trajectory(points_grid, vertex_side, bestRoute, "trajectory8ap.dat"); //route

    }
    
    /* args [0], [1] and [2] were used in previous method */
    
    static void Solve4approx(String[] args, TrpProblem[] instances){
        int N = Integer.valueOf(args[0]);
        

        int i=0, num_vertices=0, j=0, k=0, count_intersections=0, vertex_side=0, num_edges=0;
        
        //create_instance(instances, N, range1, range2);

        /* calculates the number of vertex side needed for the grid */
        // original vertex_side = getMaxNumVertexSide(instances, N);   
        vertex_side = getMaxNumVertexSide(instances, N)+1; //QUITAR!
        num_vertices = (vertex_side)*(vertex_side);
        
        /*** 1. Create the original Bar Yehuda grid and set its coordinates x,y ***/
        Grid[][] original_grid = new Grid[vertex_side][vertex_side];
        
        for(j=0; j<vertex_side; j++){
            for(i=0; i<vertex_side; i++){
                original_grid[j][i] = new Grid();
                original_grid[j][i].x = SQUARE_SIZE*i;
                original_grid[j][i].y = SQUARE_SIZE*j;
            }
        }

        
        if(LOG==1){ /* only to print the original_grid[][] for the graph*/
            for(j=0; j<vertex_side; j++){
                for(i=0; i<vertex_side; i++){
                    if(LOG==1) System.out.print(" v"+(i+(j*vertex_side))+":("+original_grid[j][i].x +","+ original_grid[j][i].y+");\t");
                }
                if(LOG==1) System.out.println();

            }
        }

         /*** 2. Create the new grid to represent the graph and set its coordinates x,y ***/
        int vertex_side_new = (vertex_side)*2;
        
        Grid[][] new_grid = new Grid[vertex_side_new][vertex_side_new];
        int i_or=0, j_or=0, idxaux=0;
        
        // starts numeration of vertex in 1 to allow create extra vertex after at 0,0
        idxaux = 1; 
        new_grid[0][0] = new Grid();
        new_grid[0][0].v = 0;
        new_grid[0][0].x = 0;
        new_grid[0][0].y = 0;
        
        /* nested 'for' for horizontals */
        for(j=0, j_or=0; j<vertex_side_new; j=j+2, j_or++){
            for(i=0, i_or=0; i<vertex_side_new; i=i+2, i_or++){
                if(j==vertex_side_new-2) continue;
                new_grid[j][i+1] = new Grid();
                new_grid[j][i+1].v = idxaux;
                
                if(i== vertex_side_new-2){
                    new_grid[j][i+1].x = new_grid[j][i-1].x + SQUARE_SIZE; // to consider exta row at right
                }else
                    new_grid[j][i+1].x = (original_grid[j_or][i_or].x + original_grid[j_or][i_or+1].x)/2;
                
                new_grid[j][i+1].y = original_grid[j_or][i_or].y;
                new_grid[j][i+1].typev = 'h';
                idxaux++;
            }
        }
        int auxvertex = idxaux; // var used to know the other vertex to join with vertex at 0,0
        /* nested 'for' for verticals */
        for(j=0, j_or=0; j<vertex_side_new; j=j+2, j_or++){
            for(i=0, i_or=0; i<vertex_side_new; i=i+2, i_or++){
                if(i==vertex_side_new-2) continue;
                new_grid[j+1][i] = new Grid();
                if(j==0 && i==0){  // guarantee that vertex 0 start in vertical and horizontal
                    new_grid[j+1][i].v = idxaux; //important
                    // idxaux--; //important
                }else
                    new_grid[j+1][i].v = idxaux;
                
                if(j== vertex_side_new-2){
                    new_grid[j+1][i].y = new_grid[j-1][i].y + SQUARE_SIZE; // to consider extra row at bottom
                }else
                    new_grid[j+1][i].y = (original_grid[j_or][i_or].y + original_grid[j_or+1][i_or].y)/2;
                
                new_grid[j+1][i].x = original_grid[j_or][i_or].x;
                new_grid[j+1][i].typev = 'v';
                idxaux++;
            }
        }
        
        if(LOG==1){/* only to print the new grid for the graph */
            System.out.println("New grid:");
            for(j=0; j<vertex_side_new; j++){
                for(i=0; i<vertex_side_new; i++){
                    if(new_grid[j][i] != null){
                        System.out.print(" v"+new_grid[j][i].v+":("+new_grid[j][i].x +","+ new_grid[j][i].y+");\t");
                    }
                }
                System.out.println();
            }
        }
        
        if(LOG==1) System.out.println("Num. segmentos: "+N);
        if(LOG==1) System.out.println("Grafo tendrá "+idxaux+" vertices");
        /*** 3. Create file to create graph ****/
        // num_edges = 2*vertex_side*(vertex_side-1);
        int new_num_vertices = idxaux; 
        // until final
        //write_file4lp(new_num_vertices, num_edges);
        String line;
        
        // vector of Strings to store temporaly the edges 
        List<String> list_edges = new ArrayList<String>();
        /* calculates the weight of each arc */
        Point p1 = new Point(0, 0);
        Point q1 = new Point(0, 0);
        Point p2 = new Point(0, 0);
        Point q2 = new Point(0, 0);
        //aux
        Point p3 = new Point(0, 0);
        Point q3 = new Point(0, 0);
        int current, target;

        for(j=0; j<vertex_side_new-1; j++){
            for(i=0; i<vertex_side_new-1; i++){
                if(new_grid[j][i] == null){
                    continue;
                }
                current = new_grid[j][i].v;
                
                /********* horizontals ********************/
                
                /** case 1: checks horizontal of current square **/ 
                if(new_grid[j][i].typev == 'h'){                                    
                    /* checks edges A_{h} (complet)*/ 
                    
                    //if(new_grid[j+1][i+1]==null)
                      //  continue;
 
                    count_intersections=0;

                    if(new_grid[j+1][i+1]!=null){
                        // p1 will be the start and end point of the horizontal side in the grid
                        p1.x=new_grid[j][i].x-SQUARE_SIZE/2; p1.y=new_grid[j][i].y;                    
                        q1.x=new_grid[j][i].x + SQUARE_SIZE/2; q1.y=new_grid[j][i].y;

                        // p3 will be the end vertical side of the grid. q1 is used as the start.
                        if(new_grid[j+1][i+1]!=null){
                            p3.x = new_grid[j+1][i+1].x; p3.y=new_grid[j+1][i+1].y + SQUARE_SIZE/2;
                            for(k=0;k<N;k++){
                                // p2 and q2 are coordinates of each slanted segment
                                p2.x= instances[k].x; p2.y =instances[k].t;
                                q2.x= instances[k].x_unit; q2.y = instances[k].t_unit;
                                if( intersect(p1, q1, p2, q2) == 1){                            
                                    // intersects horizontaly OK. now checking for vertical
                                    if( intersect(p3, q1, p2, q2) == 1){
                                        // so, intersects both horizontal and vertical of square
                                        count_intersections++; 
                                    }                   
                                }
                            }
                        }


                        //sets target to the endpoint of the slanted graph edge
                        target = new_grid[j+1][i+1].v;
                        
                        // add edge a_hv to the list
                        line = "";
                        line = current + " " + target + " " + count_intersections; 
                        list_edges.add(line);
                    
                    }  ///end if
                    
                    /** case 2: checks horizontal of right square **/    
                    if(new_grid[j][i+2]!=null){
                        //continue;
                        // p1-q1 will be the start-end point of the horizontal side in the right square
                        p1.x=new_grid[j][i+2].x-SQUARE_SIZE/2; p1.y=new_grid[j][i+2].y;                    
                        q1.x=new_grid[j][i+2].x + SQUARE_SIZE/2; q1.y=new_grid[j][i+2].y;

                        if((j-1)>=0) if(new_grid[j-1][i+1]!=null){
                            //continue;
                        
                            // p3-q3 will be the start-end vertical bottom side of the right square grid. p1 is used as the start.
                            p3.x = new_grid[j-1][i+1].x; p3.y=new_grid[j-1][i+1].y - SQUARE_SIZE/2;
                            q3.x = p1.x; q3.y=p1.y;
                            for(k=0;k<N;k++){
                                // p2 and q2 are coordinates of each slanted segment
                                p2.x= instances[k].x; p2.y =instances[k].t;
                                q2.x= instances[k].x_unit; q2.y = instances[k].t_unit;
                                if( intersect(p1, q1, p2, q2) == 1){                            
                                    // intersects horizontaly OK. now checking for vertical bottom
                                    if( intersect(p3, q3, p2, q2) == 1){
                                        // so, intersects both horizontal and vertical of square
                                        count_intersections++;                 
                                    }                   
                                }
                            }
                        }
                    }
                    
                    // add edge a_h to the list
                    target = new_grid[j][i+2].v;
                    
                    line = "";
                    line = current + " " + target + " " + count_intersections; 
                    list_edges.add(line);
                       
                }
                     
                /****** verticals ********/
                
                /** case 3: start checking verticals **/ 
                if(new_grid[j][i].typev == 'v'){                                    
                    /* checks edges A_{v} (complet)*/ 
                    count_intersections=0;

                    if(new_grid[j+1][i+1]!=null){
                        // p1 will be the start and end point of the vertical side in the grid
                        p1.x=new_grid[j][i].x; p1.y=new_grid[j][i].y-SQUARE_SIZE/2;                    
                        q1.x=new_grid[j][i].x; q1.y=new_grid[j][i].y+SQUARE_SIZE/2;

                        // p3 will be the end horizontal side of the grid. q1 is used as the start.
                        if(new_grid[j+1][i+1]!=null){
                            p3.x = new_grid[j+1][i+1].x+ SQUARE_SIZE/2; p3.y=new_grid[j+1][i+1].y;
                            q3.x = q1.x; q3.y=q1.y;

                            for(k=0;k<N;k++){
                                // p2 and q2 are coordinates of each slanted segment
                                p2.x= instances[k].x; p2.y =instances[k].t;
                                q2.x= instances[k].x_unit; q2.y = instances[k].t_unit;
                                if( intersect(p1, q1, p2, q2) == 1){                            
                                    // intersects vertical OK. now checking for horizontal
                                    if( intersect(p3, q3, p2, q2) == 1){
                                        // so, intersects both vertical and horizontal of square
                                        count_intersections++;
                                    }                   
                                }
                            }
                        }

                        //sets target to the endpoint of the slanted graph edge
                        target = new_grid[j+1][i+1].v;

                        // add edge a_vh to the list
                        line = "";
                        line = current + " " + target + " " + count_intersections; 
                        list_edges.add(line); 
                    }           
                                
                                
                                
                    
                    /** case 4: checks vertical of above square **/    
                    if((i-1)>=0) if(new_grid[j+1][i-1]!=null){
                        //continue;
                        // p1-q1 will be the start-end point of the vertical side in the above square
                        p1.x=new_grid[j+2][i].x; p1.y=new_grid[j+2][i].y-SQUARE_SIZE/2;                    
                        q1.x=new_grid[j+2][i].x; q1.y=new_grid[j+2][i].y+SQUARE_SIZE/2;
                        // p3-q3 will be the start-end horizontal left side of the above square grid. p1 is used as the start.
                        p3.x = new_grid[j+1][i-1].x - SQUARE_SIZE/2; p3.y=new_grid[j+1][i-1].y;
                        q3.x = p1.x; q3.y=p1.y;
                        for(k=0;k<N;k++){
                            // p2 and q2 are coordinates of each slanted segment
                            p2.x= instances[k].x; p2.y =instances[k].t;
                            q2.x= instances[k].x_unit; q2.y = instances[k].t_unit;
                            if( intersect(p1, q1, p2, q2) == 1){                            
                                // intersects horizontaly OK. now checking for vertical bottom
                                if( intersect(p3, q3, p2, q2) == 1){
                                    // so, intersects both horizontal and vertical of square
                                    count_intersections++;                 
                                }                   
                            }
                        }
                    }
                    // add edge a_v to the list
                    target = new_grid[j+2][i].v;
                    
                    line = "";
                    line = current + " " + target + " " + count_intersections; 
                    list_edges.add(line);
                       
                }             
            }
        }
        /* add two edges to join aux vertex at 0,0*/
        line = "";
        line = 0 + " " + 1 + " " + 0;        
        list_edges.add(line);
        line = "";
        line = 0 + " " + auxvertex + " " + 0;        
        list_edges.add(line);
        
        /* creates file to input to Java */
        num_edges = list_edges.size();
        write_file4lp(new_num_vertices, num_edges);
        add_list_file4lp(list_edges);
        
        /* Invokes to longes path function */
        if(LOG==1) System.out.println("Invoking to Longest_Path...");
        String[] args_lp = new String[2];
        args_lp[0]= "DAG.txt";
        args_lp[1]= "0";    
        
        //list to store the best route
        List<Integer> bestRoute = new ArrayList<Integer>();  
        
        invoke_longest_path(args_lp, bestRoute, 1, new_grid, vertex_side_new);
        
        //get captured8 segments
        //getNumCaptured4a(instances, N, new_grid, bestRoute, vertex_side_new);
        
        /*create files to plot segments */
        create_file_segments(instances, N);   // instances segments
        create_file_trajectory(new_grid, vertex_side_new, bestRoute, "trajectory4ap.dat");
        

    }
    /**
     * Unit tests the <tt>AcyclicLP</tt> data type.
     */
    public static void main(String[] args) {
        
        int N = Integer.valueOf(args[0]);
        float range1=Float.valueOf(args[1]);
        float range2=Float.valueOf(args[2]);
        LOG = Integer.valueOf(args[3]);
        int flagFile = Integer.valueOf(args[4]);
        
        
        /* creates instances */
        TrpProblem[] instances = new TrpProblem[N];
        if (flagFile == 1){
            readFile(instances, N, args[5]);   
        }else
            create_instance(instances, N, range1, range2, flagFile);
        
        bestObjFun = 0.0;
        Double func8ap = 0.0;
        Double func4ap = 0.0;
        Solve8approx(args, instances);        
        
        func8ap = bestObjFun;
        bestObjFun = 0.0;        
        Solve4approx(args, instances);
        func4ap = bestObjFun;
        
        System.out.println("Estadística\n8-aproximacion: " + func8ap+"\n4-aproximacion: "+func4ap + "\nRazón variación: "+(func8ap/func4ap));
    }
}
