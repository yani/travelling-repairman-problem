set autoscale
#set terminal gif color enhanced
set term postscript
set output "out4approx.ps"
# color definitions
set border linewidth 1.5
set style line 1 lc rgb '#7F7F7F' pt 7 ps 1.5 lt 1 lw 2 # --- blue
set style line 2 lc rgb '#7F7F7F' lt 1 lw 2 # --- grey

unset key

# Axes
set style line 11 lc rgb '#7F7F7F' lt 1
set border 3 back ls 11

# Custom
set xtics 0, 0.707106
set ytics 0, 0.707106


# Grid
set style line 12 lc rgb'#808080' lt 0 lw 1
set grid back ls 12

# define starting point
#x=0.; y=0.
#a=0;  b=0
#plot 'relative_data2.txt' u (x=x+$1):(y=y+$2) w steps ls 2,\
#     ''                  u (a=a+$1):(b=b+$2) w points ls 1 
plot  'graph4ap.dat' with lines ls 1 lw .001 ,\
	  'trajectory4ap.dat' with lines ls 1 lw 3 lc rgb "#0000FF",\
	  'segments.dat' with lines ls 1 lw 3 lc rgb "#FF8C00"
