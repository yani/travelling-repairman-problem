/*
 * Program that convert an instance of TRP-TW in a linear program format to input to CPLEX.
 *
 * Parameters:
 *      path: directory to puth the out file, string
 *      NumSegments: number of segments,    int
 *      range1: initial range for random coordenates generated, float
 *      range2: end range for random coordenates generated, float
 *      unit-tasks: 1 for unit-lenght of tasks, 0 for general arbitrary lenght
 *      length1: inclusive intial range for random length of tasks (only required if unit-tasks=0), int
 *      length2: inclusive final range for random length of tasks  (only required if unit-tasks=0), int
 *
 * Execution:
 *      ./linear_prog_trp.o <path> <NumSegments> <range1> <range2> <unit-tasks> <length1> <length2>
 * Sample:
 *       ./linear_prog_trp.o /Users/yani/CC/research/code/travelling-repairman-problem/linear_prog_trp/ 6 1.0 6.0 1 1.0 5.0
 *
 * Notes:
 *
 *
 * Yani. April 2015.
*/


#include <iostream>
#include <math.h>
#include <fstream>
#include "gurobi_c++.h"
#include <algorithm>    // std::minmax_element
#include <string.h>

using namespace std;

static const size_t npos = -1;
#define SQUARE_SIZE 0.707106
#define PI 3.14159265
int logg=0;

class TrpProblem {
public:
    // x: coordinate, t: time, p_i: processing time, r_i: release time,
    // d: amount of time req. to travel btw two nodes, t_i: arrival time;
    float x, t;
    float x_unit, t_unit;
    float p_i, r_i, d, t_i;

    bool operator < (const TrpProblem& a) const   { //temporaly bad
        if (x != a.x)
            return x < a.x;
        return t < a.x;
    }
};
//} *instances;

class Grid {
public:
    float x,y;
};

void create_instance(TrpProblem instances[], int N, float range1, float range2){
    srand (time(NULL));
    int i=0;

/* FOR CUSTOM INSTANCES
    instances[0].x = 0.919239;
    instances[0].t = 0.494975;

    instances[1].x = 2.75772;
    instances[1].t = 0.0707107;

    instances[2].x = 3.46482;
    instances[2].t = 1.06066;

    instances[3].x = 2.40416;
    instances[3].t = 1.83848;

    instances[4].x = 1.20208;
    instances[4].t = 0.0707107;

    instances[5].x = 2.89914;
    instances[5].t = 1.62635;

*/


    if(logg==1) cerr<<"Instances generated: "<<endl;
    for(i=0; i<N; i++){

        /* COMMENT TWO NEXT LINES FOR CUSTOM INSTANCES */
        instances[i].x   = ( (float)rand( ) / ( (float)RAND_MAX + (float)range1 ) ) * range2;
        instances[i].t   = ( (float)rand( ) / ( (float)RAND_MAX + (float)range1 ) ) * range2;

        if(logg==1) cerr<<instances[i].x<<" "<<instances[i].t<<endl;

        instances[i].x_unit = instances[i].x + cos(45 * PI / 180.0);
        instances[i].t_unit = instances[i].t + sin(45 * PI / 180.0);
        if(logg==1) cerr<<instances[i].x_unit<<" "<<instances[i].t_unit<<endl<<endl;

        instances[i].p_i = 0;
        instances[i].r_i = i/2;
        instances[i].d   = i;
        instances[i].t_i = i;
    }
}

float getRandomFloat(float Min, float Max)
{
    return ((float(rand()) / float(RAND_MAX)) * (Max - Min)) + Min;
}


void create_instance_no_unit(TrpProblem instances[], int N, float range1, float range2, float length1, float length2){
    srand (time(NULL));
    int i=0;
    float hypotenuse=0.0; //variable for represent the length of the time window

    /* FOR CUSTOM INSTANCES
    instances[0].x = 0.919239;
    instances[0].t = 0.494975;


    instances[1].x = 2.75772;
    instances[1].t = 0.0707107;

    instances[2].x = 3.46482;
    instances[2].t = 1.06066;

    instances[3].x = 2.40416;
    instances[3].t = 1.83848;

    instances[4].x = 1.20208;
    instances[4].t = 0.0707107;

    instances[5].x = 2.89914;
    instances[5].t = 1.62635;
*/

    if(logg==1) cerr<<"Instances generated: "<<endl;
    for(i=0; i<N; i++){
        /* COMMENT TWO NEXT LINES FOR CUSTOM INSTANCES*/
        instances[i].x   = ( (float)rand( ) / ( (float)RAND_MAX + (float)range1 ) ) * range2;
        instances[i].t   = ( (float)rand( ) / ( (float)RAND_MAX + (float)range1 ) ) * range2;


        if(logg==1) cerr<<instances[i].x<<" "<<instances[i].t<<endl;
        hypotenuse = 0.0;
        //works for int: temp = length2+1-length1;
        //works for int: hypotenuse = rand() % temp + length1;
        //works for float: hypotenuse = ( (float)rand( ) / ( (float)RAND_MAX + (float)length1 ) ) * length2;

        //hypotenuse=getRandomFloat(length1,length2);
        hypotenuse=2;
        instances[i].x_unit = instances[i].x + cos(45 * PI / 180.0) * hypotenuse;
        instances[i].t_unit = instances[i].t + sin(45 * PI / 180.0) * hypotenuse;
        if(logg==1) cerr<<"lenght: "<<hypotenuse<<". "<<instances[i].x_unit<<" "<<instances[i].t_unit<<endl<<endl;

        instances[i].p_i = 0;
        instances[i].r_i = i/2;
        instances[i].d   = i;
        instances[i].t_i = i;
    }


    instances[2].x_unit = instances[2].x + 3*cos(45 * PI / 180.0);
    instances[2].t_unit = instances[2].t + 3*sin(45 * PI / 180.0);
    instances[3].x_unit = instances[3].x + 3*cos(45 * PI / 180.0);
    instances[3].t_unit = instances[3].t + 3*sin(45 * PI / 180.0);

}

void readFile(TrpProblem instances[], int N, string pathFile){
    ifstream inFile;
    inFile.open(pathFile.c_str());
    string line;
    int i=0;

    while(i<N){
        getline(inFile, line);
        sscanf(line.c_str(), "%f %f", &instances[i].x, &instances[i].t);
    }
    inFile.close();

}

/*
 * Class used to represent an euclidean point composited for X,Y coordinates.
 * This object is used by intersect function.
 *
*/
class Point
{

    public :
        Point (float p_x,float p_y) : x(p_x),y(p_y){}

        float x;
        float y;

};

/*
 * Check if two line segments intersects. Return 1 if collides
 *
 *
*/
int intersect(Point a,Point b,Point c,Point d)
{
    float den = ((d.y-c.y)*(b.x-a.x)-(d.x-c.x)*(b.y-a.y));
    float num1 = ((d.x - c.x)*(a.y-c.y) - (d.y- c.y)*(a.x-c.x));
    float num2 = ((b.x-a.x)*(a.y-c.y)-(b.y-a.y)*(a.x-c.x));
    float u1 = num1/den;
    float u2 = num2/den;
    //std::cout << u1 << ":" << u2 << std::endl;
    if (den == 0 && num1  == 0 && num2 == 0)
        /* The two lines are coincidents */
        return 3;
    if (den == 0)
        /* The two lines are parallel */
        return 2;
    if (u1 <0 || u1 > 1 || u2 < 0 || u2 > 1)
        /* Lines do not collide */
        return 0;
    /* Lines DO collide */
    return 1;
}

int getMaxNumVertexSide(TrpProblem instances[], int N){
    int i=0;
    float maxy=0, maxx=0;
    int vertex_side=1;  // start in 1 since vertex in 0,0 also is counted
    float cover_area=0.0;

    /* get the max value of the y and x axis */
    maxy=instances[0].t_unit;
    maxx=instances[0].x_unit;
    for(i=0;i<N;i++){
        if(instances[i].t_unit>maxy)
            maxy=instances[i].t_unit;
        if(instances[i].x_unit>maxx)
            maxx=instances[i].x_unit;
    }
    /* take the max value of the axis */
    if(maxy > maxx)
        maxx=maxy;

    /* now maxx has the max value of both axis.
     * We calculate then the number of vertex needed */

    do{
        cover_area+=SQUARE_SIZE;
        vertex_side++;
    }while(cover_area<maxx);

    return vertex_side;
}

void restrictions_no_unit_tasks(TrpProblem instances[], int N, Grid **points_grid, int vertex_side, ofstream &myfile, int &num_restrictions, int &num_variables){
    string name_segment;
    int current=0, i=0, j=0, k=0;

    /* restriction about slanted segments */
    int flag_horizontal_and_vertical=0;
    Point p2(0, 0);
    Point q2(0, 0);
    for(k=0;k<N;k++){
        vector<string> list_intersections;

        Point p1(instances[k].x, instances[k].t), q1(instances[k].x_unit, instances[k].t_unit);
        //LOG: cout<<"coordInst:"<<instances[k].x<<","<<instances[k].t<<"Unit:"<<instances[k].x_unit<<","<<instances[k].t_unit<<endl;
        for(j=0; j<vertex_side; j++){
            for(i=0; i<vertex_side; i++){
                //cout<<" v"<<(i+(j*vertex_side))<<": ("<< points_grid[j][i].x <<","<< points_grid[j][i].y<<");";
                current = i+(j*vertex_side);
                /** start checking horizontal intersection **/
                if(i < vertex_side-1){
                    p2.x=points_grid[j][i].x; p2.y=points_grid[j][i].y;
                    q2.x=points_grid[j][i+1].x; q2.y=points_grid[j][i+1].y;  //check intersections with horizontal lines
                    if( intersect(p1, q1, p2, q2) ){
                        name_segment = "h"+to_string(current);
                        list_intersections.push_back(name_segment);
                    }
                }

                /** checks vertical intersection **/
                if(j < vertex_side-1){
                    p2.x = points_grid[j][i].x; p2.y=points_grid[j][i].y;
                    q2.x = points_grid[j+1][i].x; q2.y=points_grid[j+1][i].y;
                    if( intersect(p1, q1, p2, q2) ){
                        name_segment = "v"+to_string(current);
                        list_intersections.push_back(name_segment);
                    }
                }
            }
        }

        int aux_tam = list_intersections.size();
        if(aux_tam>=1){
            /* walk around list of intersections to create the restrictions */
            // TEMPORAL MERGED myfile<<"-2 y"<<(k+1)<<" + ";
            for(i=0; i<list_intersections.size(); i++){
                 myfile<<list_intersections.at(i);
                 if(i<(aux_tam-1)) myfile<<" + ";

            }
            // TEMPORAL myfile<<" >= -1"<<endl;
            myfile<<" - y"<<(k+1)<<" >= 0"<<endl;
            num_restrictions++;
        }

    }
}

void create_lpi_model(TrpProblem instances[], int N, Grid  **points_grid, int vertex_side, ofstream &myfile, int &num_restrictions, int &num_variables, int unit_flag){

    int i=0,j=0,k=0;
    myfile<<"MAXIMIZE "<<endl;

    for(i=1;i<=N;i++){
        myfile<<"y"<<i;
        if(i != N){
            myfile<<" + ";
        }
    }

    /* restrictions */
    myfile<<endl<<"SUBJECT TO "<<endl;
    int current = 0;
    int segments_in_grid = 2*vertex_side*(vertex_side - 1);// formula for total lines in a grid square: 2*n*(n-1)
    if(logg==1) cerr<<"total " <<segments_in_grid;

    /* general case */
    for(j=1; j<vertex_side-1; j++){
        for(i=1; i<vertex_side-1; i++){
            //cout<<" v"<<(i+(j*vertex_side))<<": ("<< points_grid[j][i].x <<","<< points_grid[j][i].y<<");";
            current = (i+(j*vertex_side));
            myfile<<"h"<<(current-1)<<" + v"<<(current-vertex_side)<<" - h"<<(current)<<" - v"<<current<<" = 0"<<endl;
            num_restrictions++;
        }
    }

    /* corner left bottom case */
    myfile<<"h0 + v0 = 1";
    myfile<<endl;
    num_restrictions++;

    /* corner right bottom case */
    myfile<<"h"<<(vertex_side-2)<<" - v"<<(vertex_side-1)<<" = 0";
    myfile<<endl;
    num_restrictions++;

    /* corner left top case */
    j=vertex_side-2;
    i=0;
    current = (i+(j*vertex_side));
    myfile<<"v"<<current<<" - h";
    j=vertex_side-1;
    i=0;
    current = (i+(j*vertex_side));
    myfile<<current<<" = 0";
    myfile<<endl;
    num_restrictions++;

    /* corner right top case */
    j=vertex_side-1;
    i=vertex_side-2;
    current = (i+(j*vertex_side));
    myfile<<"h"<<current<<" + v";
    j=vertex_side-2;
    i=vertex_side-1;
    current = (i+(j*vertex_side));
    myfile<<current<<" = 1";
    myfile<<endl;
    num_restrictions++;

    /* middle side bottom case */
    j=0;
    for(i=1; i<vertex_side-1; i++){
        current = (i+(j*vertex_side));
        myfile<<"h"<<(current-1)<<" - v"<<(current)<<" - h"<<(current)<<" = 0"<<endl;
        num_restrictions++;
    }


    /* middle side up case */
    j=vertex_side-1;
    for(i=1; i<vertex_side-1; i++){
        current = (i+(j*vertex_side));
        myfile<<"h"<<(current-1)<<" + v"<<(current-vertex_side)<<" - h"<<(current)<<" = 0"<<endl;
        num_restrictions++;
    }

    /* middle side left case */
    for(j=1; j<vertex_side-1; j++){
        i=0;
        current = (i+(j*vertex_side));
        myfile<<"v"<<(current-vertex_side)<<" - v"<<(current)<<" - h"<<(current)<<" = 0"<<endl;
        num_restrictions++;
    }

    /* middle side right case */
    i=vertex_side-1;
    for(j=1; j<vertex_side-1; j++){
        current = (i+(j*vertex_side));
        myfile<<"h"<<(current-1)<<" + v"<<(current-vertex_side)<<" - v" <<(current)<<" = 0"<<endl;
        num_restrictions++;
    }

     /* restriction 1 about slanted segments */
    for(i=1;i<=N;i++){
        myfile<<"y"<<i<<" <= 1"<<endl;
        num_restrictions++;
    }


    /* check for no-unit tasks */
    if(unit_flag==0){
        restrictions_no_unit_tasks(instances, N, points_grid, vertex_side, myfile, num_restrictions, num_variables);
    }else{

        /* Restrictions for Unit-lenght slanted segments*/
        int flag_horizontal_and_vertical=0;
        Point p2(0, 0);
        Point q2(0, 0);
        for(k=0;k<N;k++){
            Point p1(instances[k].x, instances[k].t), q1(instances[k].x_unit, instances[k].t_unit);
            if(logg==1) cerr<<"coordInst:"<<instances[k].x<<","<<instances[k].t<<"Unit:"<<instances[k].x_unit<<","<<instances[k].t_unit<<endl;
            for(j=0; j<vertex_side; j++){
                for(i=0; i<vertex_side; i++){
                    flag_horizontal_and_vertical=0;

                    /** start checking horizontal intersection **/
                    if(i == vertex_side-1) continue;
                    p2.x=points_grid[j][i].x; p2.y=points_grid[j][i].y;
                    q2.x=points_grid[j][i+1].x; q2.y=points_grid[j][i+1].y;  //check intersections with horizontal lines

                    current = i+(j*vertex_side);
                    if( intersect(p1, q1, p2, q2) ){
                        p2.x = points_grid[j][i+1].x; p2.y=points_grid[j][i+1].y;
                        q2.x = points_grid[j+1][i+1].x; q2.y=points_grid[j+1][i+1].y; //modified
                        flag_horizontal_and_vertical=1;
                        if( intersect(p1, q1, p2, q2) ){

                            // this is the case: ._|
                           //TEMPORAL myfile<<"-2 y"<<(k+1)<<" + h"<<current<<" + v"<<(current+1)<<" >= -1"<<endl;
                            myfile<<"h"<<current<<" + v"<<(current+1)<<" - y"<<(k+1) <<" >= 0"<<endl;


                            num_restrictions++;

                        }else{
                            p2.x=points_grid[j][i].x; p2.y=points_grid[j][i].y;
                            q2.x = points_grid[j-1][i].x; q2.y=points_grid[j-1][i].y;
                            if( intersect(p1, q1, p2, q2) ){

                                // this is the case: |^. and upper right line
                                //TEMPORAL myfile<<"-2 y"<<(k+1)<<" + h"<<current<<" + v"<<(current-vertex_side)<<" >= -1"<<endl;
                                myfile<<"h"<<current<<" + v"<<(current-vertex_side)<<" - y"<<(k+1)<<" >= 0"<<endl;

                                num_restrictions++;

                            }else{
                                // only intersects the horizontal segment. Check this after. PENDING.
                                //TEMPORAL myfile<<"-2 y"<<(k+1)<<" + h"<<current<<" >= -1"<<endl;
                                myfile<<"h"<<current<<" - y"<<(k+1)<<" >= 0"<<endl;

                                num_restrictions++;

                            }
                        }
                    }
                    if(j == vertex_side-1) continue; //SPECIAL ATTENTION MODIFIED
                    if(flag_horizontal_and_vertical == 0 && (p1.y > points_grid[j+1][i].y)){
                        /** checks if exists only one vertical intersection **/

                        p2.x = points_grid[j][i].x; p2.y=points_grid[j][i].y;
                        q2.x = points_grid[j+1][i].x; q2.y=points_grid[j+1][i].y;
                        if( intersect(p1, q1, p2, q2) ){
                            // this is the case: .|
                            //TEMPORAL myfile<<"-2 y"<<(k+1)<<" + v"<<current<<" >= -1"<<endl;
                            myfile<<"v"<<current<<" - y"<<(k+1)<<" >= 0"<<endl;
                            num_restrictions++;
                        }
                    }
                }
            }
        }
    }


    /* bounds */
    myfile<<"BOUNDS";


    /* binary */
    myfile<<endl<<"BINARY"<<endl;
    for(j=0; j<vertex_side; j++){
        for(i=0; i<vertex_side; i++){
            current = i+(j*vertex_side);
            if(j==i && j ==(vertex_side-1)){
                continue;
            }

            if(i==vertex_side-1){
                myfile<<"v"<<current<<endl;
                num_variables++;
                continue;
            }
            if(j==vertex_side-1){
                myfile<<"h"<<current<<endl;
                num_variables++;
                continue;
            }

            myfile<<"v"<<current<<endl;
            num_variables++;
            myfile<<"h"<<current<<endl;
            num_variables++;
        }
        if(logg==1) cerr<<endl;
    }
    for(i=1;i<=N;i++){
        myfile<<"y"<<i<<endl;
        num_variables++;
    }

    myfile<<"END";

}

int main(int argc, char** argv)
{
    srand (time(NULL)); //seed for random

    string path = *(argv+1);   // directory where is placed the solver file
    int N = atoi(*(argv+2));  //number of segments
    float range1=atof(*(argv+3));
    float range2=atof(*(argv+4));
    int unit_flag = atoi(*(argv+5));
    float length1 = atof(*(argv+6));
    float length2 = atof(*(argv+7));
    int vertex_side=0;  // used to count the vertex number of the grid
    int i=0, num_vertices=0, j=0, k=0;
    int num_restrictions = 0;
    int num_variables = 0;
    int captured_segments=0;

    TrpProblem instances[N];
    if(unit_flag == 1){
        // unit lenght tasks
        create_instance(instances, N, range1, range2);
    }else{
        // no-unit lenght tasks
        create_instance_no_unit(instances, N, range1, range2, length1, length2);

    }

    /* calculates the number of vertex side needed for the grid */
    vertex_side = getMaxNumVertexSide(instances, N);

    num_vertices = (vertex_side)*(vertex_side);
    if(logg==1) cerr << "Hello World!" << vertex_side<< endl;

    /* Create the grid and set its coordinates x,y */
    //Grid points_grid[vertex_side][vertex_side];
    Grid **points_grid = new Grid*[vertex_side];
    for(i=0;i<vertex_side; i++){
        points_grid[i] = new Grid[vertex_side];
    }

    for(j=0; j<vertex_side; j++){
        for(i=0; i<vertex_side; i++){
            points_grid[j][i].x = SQUARE_SIZE*i;
            points_grid[j][i].y = SQUARE_SIZE*j;
        }
    }

    /*COMMENTED MATRIX OF VERTEX
    for(j=0; j<vertex_side; j++){
        for(i=0; i<vertex_side; i++){
            cout<<" v"<<(i+(j*vertex_side))<<": ("<< points_grid[j][i].x <<","<< points_grid[j][i].y<<");";
        }
        cout<<endl;
    }

    clock_t begin = clock();   // checks the time start
    clock_t end = clock();     // checks the time end
    double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
    cout<<"elapsed: "<<elapsed_secs<<endl;

    */

    /********* Creates file to puts results ************/
    string name_file = path + "for_solver_"+to_string(N)+"_"+to_string(vertex_side)+".lp";
    ofstream myfile(name_file);

    create_lpi_model(instances, N, (Grid **)points_grid, vertex_side, myfile, num_restrictions, num_variables, unit_flag);
    myfile.close();

    /********* Creates the Gurobi environment and model ************/
    GRBEnv env = GRBEnv();
    if(logg==0) env.set(GRB_IntParam_LogToConsole, 0);
    GRBModel model = GRBModel(env, name_file);
    //GRBModel model = GRBModel(env, name_file);
    //model.getEnv().set(GRB_IntParam_LogToConsole, 0);


    // Optimize model
    model.optimize();
    double elapsed = model.get(GRB_DoubleAttr_Runtime);
    cout<<elapsed<<endl;

    /* Iterate over the solutions and compute the objectives */
    GRBVar* vars;
    int numvars = model.get(GRB_IntAttr_NumVars);
    vars=model.getVars();
    model.getEnv().set(GRB_IntParam_OutputFlag, 0);

    if(logg==1) cerr << endl;

    /**** write the solution ****/
    string solutionFileName = path + "for_solver_"+to_string(N)+"_"+to_string(vertex_side)+".sol";
    model.write(solutionFileName);

    /* process the solution and generates Gnuplot path */
    string varName;
    int varNameInt;
    int varValue;

    vector<int> vectorH;
    vector<int> vectorV;
    vector<int> vectorY;

    for (int j = 0; j < numvars; j++) {
      GRBVar v = vars[j];
      //cout<< "varname: "<<v.get(GRB_StringAttr_VarName);
      //cout<<" DoubleAttrX: "<< v.get(GRB_DoubleAttr_X);
      varName = v.get(GRB_StringAttr_VarName);
      varNameInt = atoi(varName.substr(1,npos).c_str());
      //cout<<endl;
      if (v.get(GRB_DoubleAttr_X) != 1 ) continue;
      if(strcmp(varName.substr(0,1).c_str(),"y")==0){
         vectorY.push_back(varNameInt);
      }

      if(strcmp(varName.substr(0,1).c_str(),"h")==0){
         vectorH.push_back(varNameInt);
      }
      if(strcmp(varName.substr(0,1).c_str(),"v")==0){
         vectorV.push_back(varNameInt);
      }

    }

    //get the objective value
    captured_segments = model.get(GRB_DoubleAttr_ObjVal);

    /* create gnuplot graphics */
    int sizeSol;
    sizeSol = (vectorH.size()>=vectorV.size())?vectorH.size():vectorV.size();
    int vertex_h =  *(min_element(vectorH.begin(), vectorH.end()));
    int vertex_v =  *(min_element(vectorV.begin(), vectorV.end()));
    char type_motion; // 'h' for horizontal, 'v' for vertical
    type_motion = (vertex_h<vertex_v)?'h':'v';
    int vertex_current = (vertex_h<vertex_v)?vertex_h:vertex_v;

    /********* Creates file for plot trajectories ************/
    string name_file_trajectories = path + "trajectories.dat";
    ofstream file_traj(name_file_trajectories);

    int delta;

    for(k=0; k<vectorH.size(); k++){
        vertex_current = vectorH.at(k);
        //file_traj<< (*(*points_grid+vertex_current)).x <<" "<< (*(*points_grid+vertex_current)).y<<endl;
        //FOR AGREGADO
        for(j=0; j<vertex_side; j++){
            for(i=0; i<vertex_side; i++){
                int temp = (i+(j*vertex_side));
                if (vertex_current == temp)
                    file_traj<< points_grid[j][i].x<<" "<< points_grid[j][i].y<<endl;
            }
        }

        delta=1;

        vertex_current = vertex_current + delta;

        //file_traj<< (*(*points_grid+vertex_current)).x <<" "<< (*(*points_grid+vertex_current)).y<<endl;
        //FOR AGREGADO
        for(j=0; j<vertex_side; j++){
            for(i=0; i<vertex_side; i++){
                int temp = (i+(j*vertex_side));
                if (vertex_current == temp)
                    file_traj<< points_grid[j][i].x<<" "<< points_grid[j][i].y<<endl;
            }
        }

        file_traj<<endl;
    }

    for(k=0; k<vectorV.size(); k++){
        vertex_current = vectorV.at(k);

        //file_traj<< (*(*points_grid+vertex_current)).x <<" "<< (*(*points_grid+vertex_current)).y<<endl;
        //FOR AGREGADO
        for(j=0; j<vertex_side; j++){
            for(i=0; i<vertex_side; i++){
                int temp = (i+(j*vertex_side));
                if (vertex_current == temp)
                    file_traj<< points_grid[j][i].x<<" "<< points_grid[j][i].y<<endl;
            }
        }

        delta=vertex_side;

        vertex_current = vertex_current + delta;
        //file_traj<< (*(*points_grid+vertex_current)).x <<" "<< (*(*points_grid+vertex_current)).y<<endl;
        //FOR AGREGADO
        for(j=0; j<vertex_side; j++){
            for(i=0; i<vertex_side; i++){
                int temp = (i+(j*vertex_side));
                if (vertex_current == temp)
                    file_traj<< points_grid[j][i].x<<" "<< points_grid[j][i].y<<endl;
            }
        }

        file_traj<<endl;
    }

    /********* Creates file for plot segments ************/
    string name_file_segments = path + "segments.dat";
    ofstream file_seg(name_file_segments);


    for(i=0; i<N; i++){
        file_seg<< instances[i].x <<" "<< instances[i].t<<endl;
        file_seg<< instances[i].x_unit <<" "<< instances[i].t_unit<<endl;
        file_seg<<endl;
    }
    cerr<<endl;
    cerr<<"----------STATISTICS----------"<<endl;
    cerr<<"Num. of Segments: "<<N<<endl;
    cerr <<"Vertex by side of grid: "<< vertex_side<<endl;
    cerr<<"Total equations generated: "<<num_restrictions<<endl;
    cerr<<"Total variables generated: "<<num_variables<<endl;
    cerr <<"Captured segments (objective value): "<< captured_segments<<endl;
    return 0;
}


void create_graphic(vector<int> Y, vector<int> H, vector<int> V, string fileName){


}
