#!/bin/bash
path=/Users/yani/CC/research/code/travelling-repairman-problem/linear_prog_trp/
numSegments=5
range1=1.0
#range2=33.6
unitTasks=1
length1=1.0
length2=1.99
customGrid=1
vertexBySide=50

plot=0 # 0: not plot, 1:yes
numTests=5
clear
clear
temp=0
#rm FILE.txt
echo "{">> FILE_unit4.txt

for i in $(seq 11 4 20)  #segments
do
	numSegments=$i

	for j in $(seq 60 4 70)   #grill size
	do
		timeAvg=0.0

		for k in $(seq 1 1 $numTests)   #tests and avg
		do
			sizeGrid=$j
			nameFile=graphic_$numSegments"_"$sizeGrid"_arbitrary.ps"
		    echo "Processing $numSegments and $sizeGrid vertex grill"    
		    temp=`echo "$sizeGrid - 2" | bc -l`
		    range2=`echo "0.707106 * $temp" | bc -l`		
			timeTemp=$(./linear_prog_trp.o ${path} ${numSegments} ${range1} ${range2} ${unitTasks} ${length1} ${length2})		
			echo "log tiempo1: $timeTemp aqui."
			timeAvg=`echo "$timeAvg + $timeTemp" | bc -l`
			if [ $plot = 1 ]; then
				echo "Gurobi ended. Plotting in Gnuplot..."
				gnuplot < to_run_solution_trp.txt
				echo "Successfully ended. See $nameFile file"
				cp out.ps $nameFile
				open $nameFile
			fi
		done
		echo "tiempo: $timeAvg"
		timeAvg=`echo "scale=2; $timeAvg / $numTests" | bc -l`
		echo "avg: $timeAvg"
		echo "{$numSegments,$j,$timeAvg}," >> FILE_unit4.txt
	done
done
echo "}">> FILE_unit4.txt
